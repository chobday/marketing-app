import express from 'express'
import Cors from 'cors'
import request from 'request'
import moment from 'moment'

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

const app = express()

//TODO whitelist
app.use(Cors())
app.use(express.static('public'))
app.use(express.json())

const router = express.Router()
app.use('/api', router)

const port = process.env.SERVER_PORT

const options = { year: '2-digit', month: '2-digit', day: 'numeric',
                hour: 'numeric', minute: '2-digit', second: '2-digit', hour12: true,
                timeZoneName: 'short' };
const britishDateTime = new Intl.DateTimeFormat('en-GB', options).format;

const startTime = new Date()

router.use((req, res, next) => {
    const currentTime = britishDateTime(Date.now())
    console.log(`${currentTime}: Message received @ ${req.originalUrl}`) //TODO add logging to file
    next()
})

router.route('/health').get((req, res) => {
    res.json({'serverStartTime': startTime } )
})

router.route('/add').post((req, res) => {
    try {
        const { email, fname, lname, phone } = req.body //TODO does not throw error if no body
        request.post(
            {
                uri: `https://us7.api.mailchimp.com/3.0/lists/${process.env.MC_LIST_ID}/members/`,
                headers: {
                    Accept: 'application/json',
                    Authorization: `Basic ${Buffer.from(`apikey:${process.env.MC_API_KEY}`).toString('base64')}`,
                },
                json: true,
                body: {
                    email_address: email,
                    status: 'subscribed',
                    email_type: 'html',
                    language: 'zh',
                    location: {
                        latitude: 10,
                        longitude: 30,
                    },
                    ip_signup: '58.182.199.99',
                    // timestamp_signup: new moment().toISOString(), //cannnot find valid  date format
                    tags: ['Volunteer', 'Has kids'],
                    merge_fields: {
                        FNAME: fname,
                        LNAME: lname,
                        PHONE: phone,
                    },
                    interests: {
                        "78c23e2f75": true, //get group id from https://us7.api.mailchimp.com/playground/
                    }
                },
            },
            (err, response, body) => {
                if (err) {
                    console.log(err)
                    reject(err);
                } else {
                    if (body.status === 'subscribed') {
                        console.log('Response from MailChimp: added ' + 
                            JSON.stringify(body).substring(1,1000))
                    } else {
                        console.log('Unexpected response from Mailchimp: ' +  JSON.stringify(body))
                    }
                }
            },       
        )
        res.json({subscribed: 1})
    } catch (err) {
        res.json({ error: err.message || err.toString() })
    }
})


app.listen(port, () => console.log(`Listening on port ${port}!`))