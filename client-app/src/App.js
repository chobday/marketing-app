import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import Grid from '@material-ui/core/Grid';
import NavBar from './components/NavBar'
import ServerStatus from './components/ServerStatus'
require('dotenv').config()


class App extends Component {

  render() {
    return (

      <div className="App">
        <NavBar />
        <Grid container spacing={24} style={{padding: 24}}>
        <Grid item xs={2}>
            <ServerStatus />
          </Grid>
          <Grid item>
            <div className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default App;
